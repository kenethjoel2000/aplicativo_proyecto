
var map = L.map('divmap').setView([-2.1782256, -79.9677535], 11.3);
L.tileLayer('https://api.maptiler.com/maps/streets/{z}/{x}/{y}.png?key=bl3ryc11zP3rJTz26Af9', {
    tileSize: 512,
    zoomOffset: -1,
    minZoom: 1,
    attribution: "\u003ca href=\"https://www.maptiler.com/copyright/\" target=\"_blank\"\u003e\u0026copy; MapTiler\u003c/a\u003e \u003ca href=\"https://www.openstreetmap.org/copyright\" target=\"_blank\"\u003e\u0026copy; OpenStreetMap contributors\u003c/a\u003e",
    crossOrigin: true
}).addTo(map);

map.addEventListener('click', function(ev) {
    document.S.DestinoLat.value = ev.latlng.lat
    document.S.DestinoLon.value = ev.latlng.lng 
});

function llenardatos(ruc, Cond ,Lat, Lon){
    document.S.Ruc.value = ruc
    document.S.Conductor.value = Cond
    document.S.Lat.value = Lat
    document.S.Lon.value = Lon
    document.getElementById('Brut').style.display = 'block';
}

function routing() {
    document.S.Bsend.style.display = 'block';
    var getwatp = L.Routing.control({
        waypoints: [
            L.latLng(document.S.Lat.value, document.S.Lon.value),
            L.latLng(document.S.DestinoLat.value, document.S.DestinoLon.value)
        ] 
    }).addTo(map);
    getwatp.on('routesfound', function (e) {
        var Mensaje = " ";
        var text = e.routes[0].instructions;
        text.forEach(function (valor) {
            Mensaje = Mensaje + valor.text+"\n";
        })
        document.S.Mensaje.value = Mensaje;
    });
    ;
}


let xhtml=new XMLHttpRequest();
xhtml.open('POST','/sensores',true);
xhtml.send();
xhtml.onreadystatechange= function(){
    if(this.readyState==4 && this.status==200){
        let data = this.responseText;
        data = JSON.parse(data);
        var x = data.sensores.Listado;
        getsensoresmap(x)
    }
}

function getsensoresmap(data){
    data.forEach( function(valor, indice, array) {
        var color = 'white';
        var op = valor.trafico;
        if(op >= 5){
            color = "green"
        }else if (op >= 2 && op < 5){
            color = 'yellow';
        }else{
            color = 'red';
        }
        var circle = L.circle([valor.latitud , valor.longitud ],{
            color: 'black',
            fillColor: color,
            fillOpacity: 0.5,
            radius: 500
        }).addTo(map);
    });
}

