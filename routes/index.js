var express = require('express');
var router = express.Router();
//-------------------------------------------------------------------------//
var fs = require('fs');
var permisos = JSON.parse(fs.readFileSync('./keys/permisos.json', 'utf8'));

//-------------------------------------------------------------------------//
//-------------------------------------------------------------------------//
var admin = require('firebase-admin');


admin.initializeApp({
    credential : admin.credential.cert(permisos),
    databaseURL : 'https://proyecto-574eb-default-rtdb.firebaseio.com'
});
var db = admin.firestore();
//-------------------------------------------------------------------------//

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('Inicio-Sesion');
});

router.post('/principal', async function(req, res, next) {
    var x = await GETFBUSUARIO(req.body);
    res.render('Principal', {user:x});
});

router.post('/sensores', async function(req, res, next) {
    var y = await GETFBSENSORES();
    res.json({sensores:y});
});

router.get('/registro', function(req, res, next) {
    res.render('Registro');
});

router.post('/send', async function(req, res, next) {
    var x = await GETVEHICULO(req.body);
    res.render('Principal', {user:x});
});

async function GETVEHICULO(usuario){
    try {
        console.log(usuario);
        var coleccion = db.collection('DATAPROYECTO').doc('/' + usuario.Ruc + '/');
        var consulta = await coleccion.get();
        var datos = consulta.data();
        console.log(datos);
        datos.Vehiculo.forEach(function (valor) {
            console.log(valor);
            if(valor.Nombre && usuario.Conductor){
                valor.Mensaje = usuario.Mensaje;
            }
        })
        console.log(datos);
        coleccion.set(datos);
        return datos;
    } catch (error) {
        console.log(error);
        return false
    }
}
async function GETFBSENSORES(){
    try {
        let coleccion = db.collection('DATAPROYECTO').doc('/' + "Sensores"+ '/');
        const consulta = await coleccion.get();
        let docs = consulta.data();
        return true;
    } catch (error) {
        return false
    }
}
async function GETFBUSUARIO(usuario){
    try {
        console.log(usuario);
        var coleccion = db.collection('DATAPROYECTO').doc('/' + usuario.Ruc + '/');
        var consulta = await coleccion.get();
        var datos = consulta.data();
        return datos;
    } catch (error) {
        console.log(error);
        return false
    }
}
//exports.indextest={
//    Vehiculo:GETVEHICULO,
//    sensores:GETFBSENSORES,
//    usuario:GETFBUSUARIO
//};
module.exports = router;