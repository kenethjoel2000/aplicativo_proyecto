var indexRouter = require('../routes/index').indextest;
var assert = require("chai").assert;

var usuario = {
    Ruc:"0955192000"
};
describe("Test Index APLICATIVO", async function(){
    await it("Vehiculo", async function(){
        var x = await indexRouter.Vehiculo(usuario);
        console.log(x);
        assert.equal(x.Nombre, "ECU911");
    });
    await it("sensores", async function(){
        var x = await indexRouter.sensores();
        assert.equal(x, true);
    });
    await it("usuario", async function(){
        var x = await indexRouter.usuario(usuario);
        assert.equal(x.Ruc, "0955192000");
    });
});